// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import Dev from './Dev'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import Pagination from '@/Pagination'
//import Pagination from '../dist/lb-vue-pagination'

Vue.component('pagination', Pagination);

new Vue({
	render: h => h(Dev)
}).$mount('#app')
