// Import vue component
import component from './src/Pagination.vue';

// To allow use as module (npm/webpack/etc.) export component
export default component;