<br><br>

<h3>lb-vue-pagination</h3>

<br>
<p><img src="https://www.leapbit.com/branding/logo.png" style="width: 300px;"></p>

<br>

## Screenshot

<p><img src="https://gitlab.com/leapbit-public/lb-vue-pagination/raw/master/screen.png" alt="lb-vue-pagination" ></p>

## Description

<p align="center">
  A Vue.js Bootstrap 4.x component using bootstrap pagination and adding search, refresh, and items per page.
</p>

<br><br><br><br><br>

## Install

``` bash
npm install lb-vue-pagination --save
```

Include plugin in your `main.js` file.

```js
import Vue from 'vue'
import lbVuePagination from 'lb-vue-pagination';
Vue.component('lb-vue-pagination', lbVuePagination);
```

## Using

template code

```html
<template>
    <lb-vue-pagination v-model="pagination" :total-count="totalCount" search-placeholder="Search" items-placeholder="Items per page"></lb-vue-pagination>
</template>
```

script code

```js
export default {
    data(){
        return {
            totalCount: 300,
            pagination: {
                query: '',
                currentPage: 1,
                limit: 20,
                initialId: [],
                searchTimeout: false
            }
        }
    },
    watch:
    {
        'pagination.query'(query)
        {
            //when someone queries, do the store reload here
        },
        'pagination.currentPage'(page)
        {
            //when someone changes the page, do the store reload here
        }
    }
};
```
<br><br>


## Available settings

| Property | Type | Required | Description |
| :---------------- | :-- | :-- | :-- |
| totalCount | int | * | Total count of all records for a pagination results |
| value (v-model) | object | * | Object in form as descripbed in example |
| searchPlaceholder | string |  | Placeholder for search, if using vue-i18n it will use key 'SEARCH', if omited will use "Search"
| itemsPlaceholder | string |  | Placeholder for items per page selector, if using vue-i18n it will use key 'ITEMSPERPAGE', if omited, will use "Items per page" |

<br><br>